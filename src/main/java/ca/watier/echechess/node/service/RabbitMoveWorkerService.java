/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.node.service;


import ca.watier.echechess.common.enums.CasePosition;
import ca.watier.echechess.common.enums.MoveType;
import ca.watier.echechess.common.enums.Side;
import ca.watier.echechess.communication.rabbitmq.configuration.RabbitMqConfiguration;
import ca.watier.echechess.communication.redis.model.GenericGameHandlerWrapper;
import ca.watier.echechess.engine.engines.GenericGameHandler;
import ca.watier.echechess.node.abstracts.AbstractMessageWorker;
import ca.watier.echechess.node.exceptions.GameNotFoundException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static ca.watier.echechess.communication.rabbitmq.configuration.RabbitMqConfiguration.ROUTING_KEY_MOVE_NODE_TO_APP;
import static ca.watier.echechess.communication.rabbitmq.configuration.RabbitMqConfiguration.TOPIC_EXCHANGE_NAME;


@Service
@RabbitListener(queues = RabbitMqConfiguration.MOVE_WORK_QUEUE_NAME)
public class RabbitMoveWorkerService extends AbstractMessageWorker {

    private final RabbitTemplate rabbitTemplate;
    private final ExecutorService executorService;
    private final GameRepositoryService gameRepositoryService;

    @Autowired
    public RabbitMoveWorkerService(GameRepositoryService gameRepositoryService,
                                   RabbitTemplate rabbitTemplate,
                                   @Value("${node.engine.thread:2}") byte nbThreadGame) {

        this.rabbitTemplate = rabbitTemplate;
        this.executorService = Executors.newFixedThreadPool(nbThreadGame);
        this.gameRepositoryService = gameRepositoryService;
    }

    /**
     * @param headers - UUID|FROM|TO|ID_PLAYER_SIDE
     * @throws GameNotFoundException
     */
    @Override
    protected void handleMessage(String[] headers) throws GameNotFoundException {
        String uuid = headers[0];
        CasePosition from = CasePosition.valueOf(headers[1]);
        CasePosition to = CasePosition.valueOf(headers[2]);
        Side playerSide = Side.getFromValue(Byte.valueOf(headers[3]));

        GenericGameHandler genericGameHandler = gameRepositoryService.getGameFromUuid(uuid);

        if (genericGameHandler == null) {
            throw new GameNotFoundException();
        }

        executorService.submit(() -> {
            MoveType moveType = genericGameHandler.movePiece(from, to, playerSide);

            if(MoveType.isMoved(moveType)) {
                String payload = uuid + '|' + from + '|' + to + '|' + moveType.getValue() + '|' + playerSide.getValue();

                gameRepositoryService.add(new GenericGameHandlerWrapper<>(uuid, genericGameHandler));
                rabbitTemplate.convertAndSend(TOPIC_EXCHANGE_NAME, ROUTING_KEY_MOVE_NODE_TO_APP, payload);
            }
        });
    }
}
