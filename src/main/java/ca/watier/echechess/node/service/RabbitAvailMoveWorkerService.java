/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.node.service;


import ca.watier.echechess.common.enums.CasePosition;
import ca.watier.echechess.common.enums.Pieces;
import ca.watier.echechess.common.enums.Side;
import ca.watier.echechess.communication.rabbitmq.configuration.RabbitMqConfiguration;
import ca.watier.echechess.engine.engines.GenericGameHandler;
import ca.watier.echechess.node.abstracts.AbstractMessageWorker;
import ca.watier.echechess.node.exceptions.GameNotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static ca.watier.echechess.communication.rabbitmq.configuration.RabbitMqConfiguration.ROUTING_KEY_AVAIL_MOVE_NODE_TO_APP;
import static ca.watier.echechess.communication.rabbitmq.configuration.RabbitMqConfiguration.TOPIC_EXCHANGE_NAME;


@Service
@RabbitListener(queues = RabbitMqConfiguration.AVAIL_MOVE_WORK_QUEUE_NAME)
public class RabbitAvailMoveWorkerService extends AbstractMessageWorker {

    private final ObjectMapper objectMapper;
    private final ExecutorService executorService;
    private final GameRepositoryService gameRepositoryService;
    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public RabbitAvailMoveWorkerService(GameRepositoryService gameRepositoryService,
                                        RabbitTemplate rabbitTemplate,
                                        ObjectMapper objectMapper,
                                        @Value("${node.engine.thread:2}") byte nbThreadGame) {

        this.objectMapper = objectMapper;
        this.executorService = Executors.newFixedThreadPool(nbThreadGame);
        this.gameRepositoryService = gameRepositoryService;
        this.rabbitTemplate = rabbitTemplate;
    }

    /**
     * UUID|FROM|ID_PLAYER_SIDE
     *
     * @throws GameNotFoundException
     */
    @Override
    protected void handleMessage(String[] headers) throws GameNotFoundException {
        String uuid = headers[0];
        CasePosition from = CasePosition.valueOf(headers[1]);
        Byte playerSideValue = Byte.valueOf(headers[2]);
        Side playerSide = Side.getFromValue(playerSideValue);
        GenericGameHandler gameFromUuid = gameRepositoryService.getGameFromUuid(uuid);

        if (gameFromUuid == null) {
            throw new GameNotFoundException();
        }

        executorService.submit(() -> {
            Map<CasePosition, Pieces> piecesLocation = gameFromUuid.getPiecesLocation();
            Pieces pieces = piecesLocation.get(from);

            List<CasePosition> positions =
                    Pieces.isKing(pieces) ?
                            gameFromUuid.getPositionKingCanMove(playerSide) :
                            gameFromUuid.getAllAvailableMoves(from, playerSide);

            if (positions.isEmpty()) {
                return;
            }

            List<String> postionsAsString = new ArrayList<>();

            for (CasePosition casePosition : positions) {
                postionsAsString.add(casePosition.name());
            }

            try {
                String payload = uuid + '|' + from + '|' + playerSideValue + '|' + objectMapper.writeValueAsString(postionsAsString);
                rabbitTemplate.convertAndSend(TOPIC_EXCHANGE_NAME, ROUTING_KEY_AVAIL_MOVE_NODE_TO_APP, payload);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
    }
}
